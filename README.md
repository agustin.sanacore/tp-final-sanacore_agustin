# TP FINAL- Sanacore_Agustin

Somos una empresa que se dedica al servicio de delivery en la zona de Palermo
A las personas que contratan nuestro servicio les damos como valor agregado un sitio web estatico en el que se ve un mapa con nuestro rango de entregas, nombre, y numero de contacto del cliente.

El Dockerfile para levantar el contenedor se encuentra en este repositorio.
Lo pueden descargar con el siguiente comando.

`wget https://gitlab.com/agustin.sanacore/tp-final-sanacore_agustin/-/raw/main/Dockerfile`

Una vez descargado, y contando con docker correctamente configurado en nuestro entorno, podemos generar el contenedor corriendo el siguiente comando.

`sudo docker build -t tp .`

Ejemplo de como lanzar el contenedor

`docker run -d -p 83:80 -e SITENAME="Empanadas10" -e WIDTH="600" -e HEIGHT="500" -e COLOR="red" -e NUMERO=46544341 --name des tp`


**Variables disponibles**
- SITENAME: Titulo para la pagina (Str)
- WIDTH: Ancho del mapa (INT)
- HEIGHT: Altura del mapa (INT)
- COLOR: Fondo de web (Ej: red, blue)
- NUMERO: Telefono de contacto. (INT)

