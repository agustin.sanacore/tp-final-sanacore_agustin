FROM ubuntu:latest
LABEL version="1"
LABEL description="Reloj en JS."

ARG DEBIAN_FRONTEND=noninteractive

RUN apt update && apt install nginx -y && apt install vim -y

# La instruccion ADD es parecida COPY
# ambos copian un fichero hacia un destino
# Pero ADD obtiene ese fichero de una url
ADD https://gitlab.com/agustin.sanacore/tp-final-sanacore_agustin/-/raw/main/index.html /var/www/html
RUN chmod +r /var/www/* -R
ADD https://gitlab.com/agustin.sanacore/tp-final-sanacore_agustin/-/raw/main/default /etc/nginx/sites-available
ADD https://gitlab.com/agustin.sanacore/tp-final-sanacore_agustin/-/raw/main/entrypoint.sh /
RUN chmod +x /entrypoint.sh

ENV SITENAME tienda
ENV WIDTH mapa1
ENV HEIGHT mapa2
ENV COLOR color
ENV NUMERO numero


EXPOSE 80 443

ENTRYPOINT ["/entrypoint.sh"]
CMD ["nginx", "-g", "daemon off;"]










